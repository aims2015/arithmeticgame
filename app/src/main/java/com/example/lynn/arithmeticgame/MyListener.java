package com.example.lynn.arithmeticgame;

import android.media.MediaPlayer;
import android.view.View;
import android.widget.Button;

import static com.example.lynn.arithmeticgame.MainActivity.*;

/**
 * Created by lynn on 6/11/2015.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            Button source = (Button)v;

            if (source == generateQuestion) {
                answer.setText("");
                message.setText("");
                correctAnswer.setText("");

                currentQuestion = Question.getQuestion();

                currentQuestion.show();
            } else if (source == submit) {
                try {
                    int userAnswer = Integer.parseInt(answer.getText().toString());
                    int correctAnswer = currentQuestion.answer();

                    if (userAnswer == correctAnswer) {
                        message.setText("Correct");
                        message.setTextColor(0xFF00AF00);

                        player = MediaPlayer.create(v.getContext(),R.raw.applause);

                        player.start();
                    } else {
                        message.setText("Incorrect");
                        message.setTextColor(0xFFFF0000);

                        player = MediaPlayer.create(v.getContext(),R.raw.boo);

                        player.start();

                        MainActivity.correctAnswer.setText("The correct answer is " + correctAnswer);
                    }
                } catch (Exception e) {
                    message.setText("The answer wasn't an integer");
                }

            }

        }

    }

}