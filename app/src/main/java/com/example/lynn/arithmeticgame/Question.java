package com.example.lynn.arithmeticgame;

/**
 * Created by lynn on 6/11/2015.
 */

public class Question {
    int operand1;
    char operator;
    int operand2;

    private Question(int operand1,
                     char operator,
                     int operand2) {
                     this.operand1 = operand1;
                     this.operator = operator;
                     this.operand2 = operand2;
    }

    public static Question getQuestion() {
        int operand1 = (int)(100*Math.random());

        char[] operators = {'+','-','*','/','%'};

        char operator = operators[(int)(operators.length*Math.random())];

        int operand2 = (int)(100*Math.random());

        return(new Question(operand1,operator,operand2));
    }

    public int answer() {
        switch (operator) {
        case '+' : return(operand1 + operand2);
        case '-' : return(operand1 - operand2);
        case '*' : return(operand1 * operand2);
        case '/' : return(operand1 / operand2);
        default : return(operand1 % operand2);
        }
    }

    public void show() {
        MainActivity.operand1.setText(String.valueOf(operand1));
        MainActivity.operator.setText(String.valueOf(operator));
        MainActivity.operand2.setText(String.valueOf(operand2));
    }
}
