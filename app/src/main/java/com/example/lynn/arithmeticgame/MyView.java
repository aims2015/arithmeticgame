package com.example.lynn.arithmeticgame;

import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static com.example.lynn.arithmeticgame.MainActivity.*;

/**
 * Created by lynn on 6/11/2015.
 */
public class MyView extends RelativeLayout {

    private Button createButton(Context context,
                                String title,
                                int width,
                                int height,
                                int textSize,
                                int id,
                                int... rules) {
       Button button = new Button(context);

       button.setId(id);

       button.setText(title);

       button.setTextSize(textSize);

       RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width,height);

       for (int counter=0;counter<rules.length;counter+=2) {
           layoutParams.addRule(rules[counter],rules[counter+1]);
       }

       button.setLayoutParams(layoutParams);

       return(button);
    }

    private EditText createEditText(Context context,
                                    String contents,
                                    int width,
                                    int height,
                                    int textSize,
                                    int id,
                                    int... rules) {
        EditText editText = new EditText(context);

        editText.setId(id);

        editText.setText(contents);

        editText.setTextSize(textSize);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width,height);

        for (int counter=0;counter<rules.length;counter+=2) {
            layoutParams.addRule(rules[counter],rules[counter+1]);
        }

        editText.setLayoutParams(layoutParams);

        return(editText);
    }

    private TextView createTextView(Context context,
                                    String contents,
                                    int width,
                                    int height,
                                    int textSize,
                                    int id,
                                    int... rules) {
        TextView textView = new TextView(context);

        textView.setId(id);

        textView.setText(contents);

        textView.setTextSize(textSize);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width,height);

        for (int counter=0;counter<rules.length;counter+=2) {
            layoutParams.addRule(rules[counter],rules[counter+1]);
        }

        textView.setLayoutParams(layoutParams);

        return(textView);
    }

    public MyView(Context context) {
        super(context);

        operand1 = createButton(context,"",200,200,40,R.id.operand1,0,0);

        addView(operand1);

        operator = createButton(context,"",200,200,40,R.id.operator,RelativeLayout.RIGHT_OF,R.id.operand1);

        addView(operator);

        operand2 = createButton(context,"",200,200,40,R.id.operand2,RelativeLayout.RIGHT_OF,R.id.operator);

        addView(operand2);

        answer = createEditText(context,"",200,200,40,R.id.answer,RelativeLayout.RIGHT_OF,R.id.operand2);

        addView(answer);

        submit = new Button(context);

        submit = createButton(context,"Submit",200,200,20,R.id.submit,RelativeLayout.RIGHT_OF,R.id.answer);

        submit.setOnClickListener(listener);

        addView(submit);

        generateQuestion = createButton(context,"Generate",LayoutParams.WRAP_CONTENT,200,20,R.id.generate,RelativeLayout.RIGHT_OF,R.id.submit);

        generateQuestion.setOnClickListener(listener);

        addView(generateQuestion);

        message = createTextView(context,"",200,100,20,R.id.message,RelativeLayout.BELOW,R.id.operand1);

        addView(message);

        correctAnswer = createTextView(context,"",500,100,20,R.id.correctanswer,RelativeLayout.RIGHT_OF,R.id.message,RelativeLayout.BELOW,R.id.operator);

        addView(correctAnswer);
    }

}
